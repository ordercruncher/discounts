<?php

namespace App\Http\Controllers;

use App\Discounts\DiscountsCalculator;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class DiscountsController extends Controller
{
    protected $discountsCalculator;

    /**
     * Create new DiscountsController object.
     *
     * @param App\Discounts\DiscountsCalculator $discounts_calculator Discounts calculator.
     *
     * @return void
     */
    public function __construct(DiscountsCalculator $discounts_calculator)
    {
        $this->discountsCalculator = $discounts_calculator;
    }

    /**
     * Calculate discounts [POST /discounts].
     *
     * @param Request $request Request containing Order data.
     *
     * @return string JSON string with discounts and updated Order.
     */
    public function calculate(Request $request)
    {
        $this->validate(
            $request,
            [
                'id' => 'required|integer',
                'customer-id' => 'required|integer',
                'items' => 'required|array',
                'items.*.product-id' => 'required|regex:#^\D\d+$#',
                'items.*.quantity' => 'required|integer',
                'items.*.unit-price' => 'required|regex:#^\d+\.\d{2}$#',
                'items.*.total' => 'required|regex:#^\d+\.\d{2}$#',
                'total' => 'required|regex:#^\d+\.\d{2}$#',
            ]
        );
        return response()->json(
            $this->discountsCalculator->forOrder($request->all())
        );
    }
}
