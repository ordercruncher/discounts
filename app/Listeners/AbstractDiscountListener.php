<?php

namespace App\Listeners;

use App\Events\OrderReceived;
use App\Repositories\CustomerRepository;
use App\Repositories\ProductRepository;

abstract class AbstractDiscountListener
{
    protected $customerRepository;
    protected $productRepository;

    /**
     * Create a new OrderReceived event listener.
     *
     * @param App\Repositories\CustomerRepository $customer_repository Customer repository.
     * @param App\Repositories\ProductRepository $product_repository Product repository.
     *
     * @return void
     */
    public function __construct(CustomerRepository $customer_repository, ProductRepository $product_repository)
    {
        $this->customerRepository = $customer_repository;
        $this->productRepository = $product_repository;
    }

    /**
     * Handle the event.
     *
     * @param App\Events\OrderReceived $event Order received event.
     *
     * @return void
     */
    abstract public function handle(OrderReceived $event);
}
