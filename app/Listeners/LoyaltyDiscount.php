<?php

namespace App\Listeners;

use App\Events\OrderReceived;

class LoyaltyDiscount extends AbstractDiscountListener
{
    const MIN_REVENUE = 1000;
    const DISCOUNT_RATE = 0.1;

    /**
     * {@inheritdoc}
     */
    public function handle(OrderReceived $event)
    {
        $customer_id = $event->getCustomerId();
        $customer_data = $this->customerRepository->find($customer_id);

        if (is_null($customer_data) || $customer_data['revenue'] < self::MIN_REVENUE) {
            return;
        }

        $event->addDiscount(
            'loyalty',
            [
                'min-revenue' => self::MIN_REVENUE,
                'discount' => sprintf('-%d%%', self::DISCOUNT_RATE * 100),
            ]
        )->setUpdatedTotal(round($event->getUpdatedTotal() * (1 - self::DISCOUNT_RATE), 2));
    }
}
