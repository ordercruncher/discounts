<?php

namespace App\Listeners;

use App\Events\OrderReceived;

class FreeItemsDiscount extends AbstractDiscountListener
{
    const CATEGORY_IDS = [2];
    const MIN_ITEMS = 5;
    const FREE_COUNT = 1;

    /**
     * {@inheritdoc}
     */
    public function handle(OrderReceived $event)
    {
        $total_discount = 0;

        foreach ($event->getItems() as $delta => $item) {
            if ($item['quantity'] < self::MIN_ITEMS + self::FREE_COUNT) {
                // Quantity must be at least the minimum required number of items
                // plus the number of free items. We do not add free items to the
                // quantity ourselves - clients must do this.
                continue;
            }

            $product_data = $this->productRepository->find($item['product-id']);

            if (is_null($product_data) || !in_array($product_data['category'], self::CATEGORY_IDS)) {
                continue;
            }

            $discount = $product_data['price'] * self::FREE_COUNT;
            $total_discount += $discount;
            $event->addDiscount(
                'free-items',
                [
                    'category' => $product_data['category'],
                    'min-items' => self::MIN_ITEMS,
                    'free-count' => self::FREE_COUNT,
                    'product-id' => $item['product-id'],
                ]
            )->getUpdatedItem($delta)['total'] -= $discount;
        }

        if ($total_discount > 0) {
            $event->setUpdatedTotal($event->getUpdatedTotal() - $total_discount);
        }
    }
}
