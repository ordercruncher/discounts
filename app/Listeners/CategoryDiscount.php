<?php

namespace App\Listeners;

use App\Events\OrderReceived;

class CategoryDiscount extends AbstractDiscountListener
{
    const CATEGORY_IDS = [1];
    const MIN_PRODUCTS = 2;
    const DISCOUNT_RATE = 0.2;

    /**
     * {@inheritdoc}
     */
    public function handle(OrderReceived $event)
    {
        $total_discount = 0;

        foreach ($this->getItemsByCategory($event->getItems()) as $category_id => $category_items) {
            if (count($category_items) < self::MIN_PRODUCTS) {
                continue;
            }

            $cheapest_delta = $this->getCheapestItem($category_items);
            $cheapest_product = $event->getItem($cheapest_delta);

            $discount = $cheapest_product['total'] * self::DISCOUNT_RATE;
            $total_discount += $discount;
            $event->addDiscount(
                'category',
                [
                    'category' => $category_id,
                    'min-products' => self::MIN_PRODUCTS,
                    'discount' => sprintf('-%d%%', self::DISCOUNT_RATE * 100),
                    'product-id' => $cheapest_product['product-id'],
                ]
            )->getUpdatedItem($cheapest_delta)['total'] -= $discount;
        }

        if ($total_discount > 0) {
            $event->setUpdatedTotal($event->getUpdatedTotal() - $total_discount);
        }
    }

    /**
     * Get items by category with the ID as key and price as value.
     *
     * @param array $items Order items.
     *
     * @return array Items by category.
     */
    protected function getItemsByCategory(array $items): array
    {
        $items_by_category = [];
        foreach ($items as $delta => $item) {
            $product_data = $this->productRepository->find($item['product-id']);

            if (is_null($product_data) || !in_array($product_data['category'], self::CATEGORY_IDS)) {
                continue;
            }

            // Save per category the item delta to unit price.
            $items_by_category[$product_data['category']][$delta] = $product_data['price'];
        }
        return $items_by_category;
    }

    /**
     * Get the cheapest item from a category returned from getItemsByCategory().
     *
     * @param array $items Order items.
     *
     * @return int Item ID (delta).
     */
    protected function getCheapestItem(array $items): int
    {
        // Values are prices - get the lowest.
        $cheapest_price = min($items);
        // Get the delta of the lowest price.
        return current(array_keys($items, $cheapest_price));
    }
}
