<?php

namespace App\Repositories;

use GuzzleHttp\Exception\RequestException;

class ProductRestRepository extends AbstractRestRepository implements ProductRepository
{
    /**
     * {@inheritdoc}
     */
    public function find(string $id): ?array
    {
        try
        {
            return json_decode($this->httpClient->request('GET', '/products/' . $id)->getBody(), true);
        }
        catch (RequestException $e)
        {
            return null;
        }
    }
}
