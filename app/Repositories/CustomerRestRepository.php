<?php

namespace App\Repositories;

use GuzzleHttp\Exception\RequestException;

class CustomerRestRepository extends AbstractRestRepository implements CustomerRepository
{
    /**
     * {@inheritdoc}
     */
    public function find(int $id): ?array
    {
        try
        {
            return json_decode($this->httpClient->request('GET', '/customers/' . $id)->getBody(), true);
        }
        catch (RequestException $e)
        {
            return null;
        }
    }
}
