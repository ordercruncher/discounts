<?php

namespace App\Repositories;

interface CustomerRepository
{
    /**
     * Find Customer with given ID.
     *
     * @param int $id Customer ID.
     *
     * @return array|null Customer data.
     */
    public function find(int $id): ?array;
}
