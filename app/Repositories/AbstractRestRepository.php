<?php

namespace App\Repositories;

use GuzzleHttp\Client;

abstract class AbstractRestRepository
{
    protected $httpClient;

    /**
     * Create instance.
     *
     * @param GuzzleHttp\Client $http_client HTTP client.
     */
    public function __construct(Client $http_client)
    {
        $this->httpClient = $http_client;
    }
}
