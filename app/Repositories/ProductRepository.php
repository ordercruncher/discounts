<?php

namespace App\Repositories;

interface ProductRepository
{
    /**
     * Find Product with given ID.
     *
     * @param string $id Product ID.
     *
     * @return array|null Product data.
     */
    public function find(string $id): ?array;
}
