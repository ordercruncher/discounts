<?php

namespace App\Discounts;

use App\Events\OrderReceived;
use Illuminate\Support\Facades\Event;

class DiscountsCalculator
{
    /**
     * Calculate discounts and the updated order for a given order.
     *
     * @param array $order Received order data.
     *
     * @return array Discounts and updated order.
     */
    public function forOrder(array $order): array
    {
        $event = new OrderReceived($order);
        event($event);
        return $event->toArray();
    }
}
