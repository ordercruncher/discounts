<?php

namespace App\Events;

class OrderReceived
{
    protected $order;
    protected $discounts = [];
    protected $updatedOrder;

    /**
     * Create a new OrderReceived object.
     *
     * @param array $order Order data.
     *
     * @return void
     */
    public function __construct(array $order)
    {
        $this->order = $order;
        $this->updatedOrder = $order;
    }

    /**
     * Get the Order customer ID.
     *
     * @return int Customer ID.
     */
    public function getCustomerId(): int
    {
        return (int) $this->order['customer-id'];
    }

    /**
     * Get the Order's items.
     *
     * @return array Items.
     */
    public function getItems(): array
    {
        return $this->order['items'];
    }

    /**
     * Get an Order's item.
     *
     * @param int $delta Delta.
     *
     * @return array|null Item data or null.
     */
    public function getItem(int $delta): ?array
    {
        return $this->order['items'][$delta] ?? null;
    }

    /**
     * Add a discount.
     *
     * @param string $type     Discount type.
     * @param array  $discount Discount data.
     *
     * @return App\Events\OrderReceived The OrderReceived object.
     */
    public function addDiscount(string $type, array $discount): OrderReceived
    {
        $this->discounts[] = ['type' => $type] + $discount;
        return $this;
    }

    /**
     * Get all discounts.
     *
     * @return array Discounts.
     */
    public function getDiscounts(): array
    {
        return $this->discounts;
    }

    /**
     * Get an Order's updated item.
     *
     * @param int $delta Delta.
     *
     * @return array Updated item data (by reference).
     */
    public function &getUpdatedItem(int $delta): ?array
    {
        return $this->updatedOrder['items'][$delta];
    }

    /**
     * Get the Order's updated total.
     *
     * @return float Order total.
     */
    public function getUpdatedTotal(): float
    {
        return (float) $this->updatedOrder['total'];
    }

    /**
     * Set the Order's updated total.
     *
     * @param float $total Order total.

     * @return void
     */
    public function setUpdatedTotal(float $total)
    {
        $this->updatedOrder['total'] = $total;
    }

    /**
     * Serialize to array.
     *
     * @return array Data.
     */
    public function toArray(): array
    {
        return [
            'discounts' => $this->discounts,
            'updated-order' => $this->updatedOrder,
        ];
    }
}
