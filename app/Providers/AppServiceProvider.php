<?php

namespace App\Providers;

use App\Repositories\CustomerRestRepository;
use App\Repositories\ProductRestRepository;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'ServicesRestClient', function ($app) {
                return new Client(
                    [
                        'base_uri' => env('SERVICES_URL'),
                    ]
                );
            }
        );
        $this->app->bind(
            'App\Repositories\CustomerRepository', function ($app) {
                return new CustomerRestRepository($app->make('ServicesRestClient'));
            }
        );
        $this->app->bind(
            'App\Repositories\ProductRepository', function ($app) {
                return new ProductRestRepository($app->make('ServicesRestClient'));
            }
        );
    }
}
