<?php

use App\Discounts\DiscountsCalculator;

class DiscountsControllerTest extends TestCase
{
    protected $discountsCalculator;
    protected $order;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();
        $this->discountsCalculator = Mockery::mock(DiscountsCalculator::class);
        $this->app->instance(DiscountsCalculator::class, $this->discountsCalculator);
        $this->order = json_decode(file_get_contents(__DIR__ . '/orders/order1.json'), true);
    }

    /**
     * Test the Order is passed to the DiscountsCalculator.
     *
     * @return void
     */
    public function testCalculate()
    {
        $order = $this->order;
        $this->discountsCalculator->shouldReceive('forOrder')->once()
            ->with($order)->andReturn($order);
        $this->json('POST', '/discounts', $order)->seeJsonEquals($order);
    }

    /**
     * Test whether missing order data is rejected.
     *
     * @return void
     */
    public function testMissingOrderData()
    {
        $response = $this->json('POST', '/discounts', [])
            ->seeJson(
                ['id' => ['The id field is required.']]
            )
            ->seeJson(
                ['customer-id' => ['The customer-id field is required.']]
            )
            ->seeJson(
                ['items' => ['The items field is required.']]
            )
            ->seeJson(
                ['total' => ['The total field is required.']]
            );
    }

    /**
     * Test whether invalid order data is rejected.
     *
     * @return void
     */
    public function testInvalidOrderData()
    {
        $order = [
            'id' => 'A',
            'customer-id' => 'A',
            'items' => 'A',
            'total' => '100',
        ];
        $response = $this->json('POST', '/discounts', $order)
            ->seeJson(
                ['id' => ['The id must be an integer.']]
            )
            ->seeJson(
                ['customer-id' => ['The customer-id must be an integer.']]
            )
            ->seeJson(
                ['items' => ['The items must be an array.']]
            )
            ->seeJson(
                ['total' => ['The total format is invalid.']]
            );
    }

    /**
     * Test whether missing item data is rejected.
     *
     * @return void
     */
    public function testMissingItemData()
    {
        $order = $this->order;
        $order['items'][0] = [];
        $response = $this->json('POST', '/discounts', $order)
            ->seeJson(
                ['items.0.product-id' => ['The items.0.product-id field is required.']]
            )
            ->seeJson(
                ['items.0.quantity' => ['The items.0.quantity field is required.']]
            )
            ->seeJson(
                ['items.0.unit-price' => ['The items.0.unit-price field is required.']]
            )
            ->seeJson(
                ['items.0.total' => ['The items.0.total field is required.']]
            );
    }

    /**
     * Test whether invalid item data is rejected.
     *
     * @return void
     */
    public function testInvalidItemData()
    {
        $order = $this->order;
        $order['items'][0] = [
            'product-id' => '1',
            'quantity' => 'A',
            'unit-price' => '100',
            'total' => '100',
        ];
        $response = $this->json('POST', '/discounts', $order)
            ->seeJson(
                ['items.0.product-id' => ['The items.0.product-id format is invalid.']]
            )
            ->seeJson(
                ['items.0.quantity' => ['The items.0.quantity must be an integer.']]
            )
            ->seeJson(
                ['items.0.unit-price' => ['The items.0.unit-price format is invalid.']]
            )
            ->seeJson(
                ['items.0.total' => ['The items.0.total format is invalid.']]
            );
    }
}
