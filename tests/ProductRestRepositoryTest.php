<?php

use App\Repositories\ProductRestRepository;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\RequestInterface;

class ProductRestRepositoryTest extends AbstractRestRepositoryTest
{
    /**
     * Test product repository.
     *
     * @param string         $id        Product ID.
     * @param string         $json      JSON data.
     * @param Exception|null $exception Exception thrown.
     * @param mixed          $expected  Expected result.
     *
     * @dataProvider productRepositoryProvider
     *
     * @return void
     */
    public function testProductRepository(string $id, string $json, ?Exception $exception, $expected)
    {
        $this->setUpHttpClient('/products/' . $id, $json, $exception);

        $product_repository = new ProductRestRepository($this->httpClient);
        $product = $product_repository->find($id);
        $this->assertEquals($expected, $product);
    }

    /**
     * Data provider for testProductRepository.
     *
     * @return array Test data.
     */
    public function productRepositoryProvider(): array
    {
        $exception = new RequestException('Foo', Mockery::mock(RequestInterface::class));
        return [
            ['A101', '{"id": "A101", "category": "1", "price": "9.75"}', null, [
                'id' => 'A101',
                'category' => '1',
                'price' => '9.75',
            ]],
            ['A102', '', $exception, null],
        ];
    }
}
