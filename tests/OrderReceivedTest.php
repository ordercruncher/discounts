<?php

use App\Events\OrderReceived;

class OrderReceivedTest extends TestCase
{
    protected $order;
    protected $orderReceived;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();
        $this->order = json_decode(file_get_contents(__DIR__ . '/orders/order1.json'), true);
        $this->orderReceived = new OrderReceived($this->order);
    }

    /**
     * Test getters.
     *
     * @return void
     */
    public function testGetters()
    {
        $this->assertEquals($this->order['customer-id'], $this->orderReceived->getCustomerId());
        $this->assertEquals($this->order['items'], $this->orderReceived->getItems());
        $this->assertEquals($this->order['items'][0], $this->orderReceived->getItem(0));
        $this->assertNull($this->orderReceived->getItem(1));
    }

    /**
     * Test discounts.
     *
     * @return void
     */
    public function testDiscounts()
    {
        $this->assertEquals([], $this->orderReceived->getDiscounts());
        $this->orderReceived->addDiscount(
            'foobar',
            [
                'foo' => 'bar',
                'bar' => 'baz',
            ]
        );
        $this->orderReceived->addDiscount(
            'another',
            [
                'bono' => 'estente',
            ]
        );
        $this->assertEquals(
            [
                [
                    'type' => 'foobar',
                    'foo' => 'bar',
                    'bar' => 'baz',
                ],
                [
                    'type' => 'another',
                    'bono' => 'estente',
                ],
            ],
            $this->orderReceived->getDiscounts()
        );
    }

    /**
     * Test updated items.
     *
     * @return void
     */
    public function testUpdatedItems()
    {
        $original_item = $this->order['items'][0];
        // By reference.
        $item = &$this->orderReceived->getUpdatedItem(0);
        $this->assertEquals($original_item, $item);

        // Add 100 to total of item.
        $item['total'] += 100;
        $original_item['total'] += 100;

        // Newly returned item from updated order is updated.
        $item = $this->orderReceived->getUpdatedItem(0);
        $this->assertEquals($original_item, $item);

        // Non existing.
        $this->assertNull($this->orderReceived->getUpdatedItem(1));
    }

    /**
     * Test updated total.
     *
     * @return void
     */
    public function testUpdatedTotal()
    {
        $this->assertEquals($this->order['total'], $this->orderReceived->getUpdatedTotal());
        $this->orderReceived->setUpdatedTotal($this->order['total'] + 200);
        $this->assertEquals($this->order['total'] + 200, $this->orderReceived->getUpdatedTotal());
    }

    /**
     * Test array serialization.
     *
     * @return void
     */
    public function testArraySerialize()
    {
        $order = $this->order;
        $order['items'][0]['total'] += 100;
        $order['total'] += 200;

        $this->orderReceived->addDiscount(
            'foobar',
            [
                'foo' => 'bar',
                'bar' => 'baz',
            ]
        );
        $this->orderReceived->getUpdatedItem(0)['total'] += 100;
        $this->orderReceived->setUpdatedTotal($order['total']);

        $this->assertEquals(
            [
                'discounts' => [
                    [
                        'type' => 'foobar',
                        'foo' => 'bar',
                        'bar' => 'baz',
                    ],
                ],
                'updated-order' => $order,
            ],
            $this->orderReceived->toArray()
        );
    }
}
