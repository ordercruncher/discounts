<?php

use App\Discounts\DiscountsCalculator;

class DiscountsCalculatorTest extends TestCase
{
    /**
     * Test DiscountCalculator::forOrder().
     *
     * @return void
     */
    public function testForOrder()
    {
        $this->expectsEvents('App\Events\OrderReceived');
        $order = json_decode(file_get_contents(__DIR__ . '/orders/order1.json'), true);
        $result = (new DiscountsCalculator())->forOrder($order);
        $this->assertEquals(
            [
                'discounts' => [],
                'updated-order' => $order,
            ],
            $result
        );
    }
}
