<?php

use App\Events\OrderReceived;
use App\Listeners\LoyaltyDiscount;

class LoyaltyDiscountTest extends AbstractDiscountTest
{
    /**
     * Test granting loyalty discounts.
     *
     * @param array|null $customer   Customer data.
     * @param bool       $discounted Discounted.
     *
     * @dataProvider loyaltyDiscountProvider
     *
     * @return void
     */
    public function testLoyaltyDiscount(?array $customer, bool $discounted)
    {
        $order = $this->getOrder(2);
        $order_received = new OrderReceived($order);

        $this->customerRepository->shouldReceive('find')->with(2)->andReturn($customer);

        $loyalty = new LoyaltyDiscount($this->customerRepository, $this->productRepository);
        $loyalty->handle($order_received);

        if ($discounted) {
            $this->assertNotEmpty($order_received->getDiscounts());
            $this->assertContains(
                [
                    'type' => 'loyalty',
                    'min-revenue' => 1000,
                    'discount' => '-10%',
                ],
                $order_received->getDiscounts()
            );
            $order['total'] = 22.46;
        } else {
            $this->assertEmpty($order_received->getDiscounts());
        }
        $this->assertEquals($order['items'][0], $order_received->getUpdatedItem(0));
        $this->assertEquals($order['total'], $order_received->getUpdatedTotal());
    }

    /**
     * Data provider for testLoyaltyDiscount.
     *
     * @return array Test data.
     */
    public function loyaltyDiscountProvider(): array
    {
        return [
            [['revenue' => '0'], false],
            [['revenue' => '999'], false],
            [['revenue' => '999.99'], false],
            [['revenue' => '1000'], true],
            [['revenue' => '1000.01'], true],
            [['revenue' => '1001'], true],
            // Non existing customer.
            [null, false],
        ];
    }
}
