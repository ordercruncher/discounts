<?php

use App\Events\OrderReceived;
use App\Listeners\FreeItemsDiscount;

class FreeItemsDiscountTest extends AbstractDiscountTest
{
    /**
     * Test different product data on free items discount.
     *
     * @param array|null $product    Product data.
     * @param bool       $discounted Discounted.
     * @param float      $total      Total.
     *
     * @dataProvider differentProductDataProvider
     *
     * @return void
     */
    public function testDifferentProductData(?array $product, bool $discounted, float $total)
    {
        $order = $this->getOrder(1);
        $order_received = new OrderReceived($order);

        $this->productRepository->shouldReceive('find')->with('B102')->andReturn($product);

        $free_items = new FreeItemsDiscount($this->customerRepository, $this->productRepository);
        $free_items->handle($order_received);

        if ($discounted) {
            $this->assertNotEmpty($order_received->getDiscounts());
            $this->assertContains(
                [
                    'type' => 'free-items',
                    'category' => 2,
                    'min-items' => 5,
                    'free-count' => 1,
                    'product-id' => 'B102',
                ],
                $order_received->getDiscounts()
            );
            $order['items'][0]['total'] = $order['total'] = $total;
        } else {
            $this->assertEmpty($order_received->getDiscounts());
        }
        $this->assertEquals($order['items'][0], $order_received->getUpdatedItem(0));
        $this->assertEquals($order['total'], $order_received->getUpdatedTotal());
    }

    /**
     * Data provider for testDifferentProductData.
     *
     * @return array Test data.
     */
    public function differentProductDataProvider(): array
    {
        return [
            [['category' => 2, 'price' => 4.99], true, 44.91],
            [['category' => 1, 'price' => 4.99], false, 0],
            [['category' => 2, 'price' => 3.99], true, 45.91],
            // Non existing product.
            [null, false, 0],
        ];
    }

    /**
     * Test different quantities on free items discount.
     *
     * @param int  $quantity   Quantity.
     * @param bool $discounted Discounted.
     *
     * @dataProvider differentQuantitiesProvider
     *
     * @return void
     */
    public function testDifferentQuantities(int $quantity, bool $discounted)
    {
        $order = $this->getOrder(1);
        $order['items'][0]['quantity'] = $quantity;
        $order_received = new OrderReceived($order);

        $this->productRepository->shouldReceive('find')->with('B102')
            ->andReturn(['category' => 2, 'price' => 4.99]);

        $free_items = new FreeItemsDiscount($this->customerRepository, $this->productRepository);
        $free_items->handle($order_received);

        if ($discounted) {
            $this->assertNotEmpty($order_received->getDiscounts());
            $this->assertContains(
                [
                    'type' => 'free-items',
                    'category' => 2,
                    'min-items' => 5,
                    'free-count' => 1,
                    'product-id' => 'B102',
                ],
                $order_received->getDiscounts()
            );
            $this->assertEquals($quantity, $order_received->getUpdatedItem(0)['quantity']);
            $this->assertEquals(44.91, $order_received->getUpdatedItem(0)['total']);
            $this->assertEquals(44.91, $order_received->getUpdatedTotal());
        } else {
            $this->assertEmpty($order_received->getDiscounts());
            $this->assertEquals($quantity, $order_received->getUpdatedItem(0)['quantity']);
            $this->assertEquals($order['items'][0]['total'], $order_received->getUpdatedItem(0)['total']);
            $this->assertEquals($order['total'], $order_received->getUpdatedTotal());
        }
    }

    /**
     * Data provider for testDifferentQuantities.
     *
     * @return array Test data.
     */
    public function differentQuantitiesProvider(): array
    {
        return [
            [4, false],
            // Must have the free item already in order.
            [5, false],
            // The free item is included in the order.
            [6, true],
            [9, true],
            [10, true],
            [11, true],
        ];
    }
}
