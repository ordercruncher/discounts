<?php

use App\Repositories\CustomerRestRepository;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\RequestInterface;

class CustomerRestRepositoryTest extends AbstractRestRepositoryTest
{
    /**
     * Test customer repository.
     *
     * @param int            $id        Customer ID.
     * @param string         $json      JSON data.
     * @param Exception|null $exception Exception thrown.
     * @param mixed          $expected  Expected result.
     *
     * @dataProvider customerRepositoryProvider
     *
     * @return void
     */
    public function testCustomerRepository(int $id, string $json, ?Exception $exception, $expected)
    {
        $this->setUpHttpClient('/customers/' . $id, $json, $exception);

        $customer_repository = new CustomerRestRepository($this->httpClient);
        $customer = $customer_repository->find($id);
        $this->assertEquals($expected, $customer);
    }

    /**
     * Data provider for testCustomerRepository.
     *
     * @return array Test data.
     */
    public function customerRepositoryProvider(): array
    {
        $exception = new RequestException('Foo', Mockery::mock(RequestInterface::class));
        return [
            [1, '{"id": "1", "name": "Coca Cola", "revenue": "492.12"}', null, [
                'id' => '1',
                'name' => 'Coca Cola',
                'revenue' => '492.12',
            ]],
            [2, '', $exception, null],
        ];
    }
}
