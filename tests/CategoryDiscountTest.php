<?php

use App\Events\OrderReceived;
use App\Listeners\CategoryDiscount;

class CategoryDiscountTest extends AbstractDiscountTest
{
    /**
     * Test different product data on category discount.
     *
     * @param array       $products   Products data.
     * @param string|null $discounted Discounted product ID.
     * @param array       $totals     Totals.
     *
     * @dataProvider differentProductDataProvider
     *
     * @return void
     */
    public function testDifferentProductData(array $products, ?string $discounted, array $totals)
    {
        $order = $this->getOrder(3);
        $order_received = new OrderReceived($order);

        foreach ($products as $id => $data) {
            $this->productRepository->shouldReceive('find')->with($id)->andReturn($data);
        }

        $category = new CategoryDiscount($this->customerRepository, $this->productRepository);
        $category->handle($order_received);

        if (!is_null($discounted)) {
            $this->assertNotEmpty($order_received->getDiscounts());
            $this->assertContains(
                [
                    'type' => 'category',
                    'category' => 1,
                    'min-products' => 2,
                    'discount' => '-20%',
                    'product-id' => $discounted,
                ],
                $order_received->getDiscounts()
            );
            foreach ($order['items'] as $delta => $item) {
                $item['total'] = $totals[$delta];
                $this->assertEquals($item, $order_received->getUpdatedItem($delta));
            }
        } else {
            $this->assertEmpty($order_received->getDiscounts());
            foreach ($order['items'] as $delta => $item) {
                $this->assertEquals($item, $order_received->getUpdatedItem($delta));
            }
        }
        $this->assertEquals(array_sum($totals), $order_received->getUpdatedTotal());
    }

    /**
     * Data provider for testDifferentProductData.
     *
     * @return array Test data.
     */
    public function differentProductDataProvider(): array
    {
        return [
            // Same categories.
            [[
                'A101' => [
                    'id' => 'A101',
                    'description' => 'Screwdriver',
                    'category' => '1',
                    'price' => '9.75',
                ],
                'A102' => [
                    'id' => 'A102',
                    'description' => 'Electric screwdriver',
                    'category' => '1',
                    'price' => '49.50',
                ],
            ], 'A101', [15.60, 49.50]],
            // Different prices (A102 cheapest).
            [[
                'A101' => [
                    'id' => 'A101',
                    'description' => 'Screwdriver',
                    'category' => '1',
                    'price' => '49.50',
                ],
                'A102' => [
                    'id' => 'A102',
                    'description' => 'Electric screwdriver',
                    'category' => '1',
                    'price' => '9.75',
                ],
            ], 'A102', [19.50, 39.60]],
            // Different categories - no discount.
            [[
                'A101' => [
                    'id' => 'A101',
                    'description' => 'Screwdriver',
                    'category' => '1',
                    'price' => '9.75',
                ],
                'A102' => [
                    'id' => 'A102',
                    'description' => 'Electric screwdriver',
                    'category' => '2',
                    'price' => '49.50',
                ],
            ], null, [19.50, 49.50]],
        ];
    }
}
