<?php

use App\Repositories\CustomerRepository;
use App\Repositories\ProductRepository;

abstract class AbstractDiscountTest extends TestCase
{
    protected $customerRepository;
    protected $productRepository;

    /**
     * Set up mocked repositories.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->customerRepository = Mockery::mock(CustomerRepository::class);
        $this->productRepository = Mockery::mock(ProductRepository::class);
    }

    /**
     * Get example Order data from JSON file.
     *
     * @param int $id Order ID.
     *
     * @return array Order data.
     */
    protected function getOrder(int $id): array
    {
        return json_decode(file_get_contents(__DIR__ . '/orders/order' . $id . '.json'), true);
    }
}
