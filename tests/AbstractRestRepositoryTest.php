<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

abstract class AbstractRestRepositoryTest extends TestCase
{
    protected $httpClient;
    protected $response;

    /**
     * Set up mocked HTTP client.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->httpClient = Mockery::mock(Client::class);
        $this->response = Mockery::mock(Response::class);
    }

    /**
     * Set up mocked HTTP client responses.
     *
     * @param string         $path      URI path.
     * @param string         $json      JSON data.
     * @param Exception|null $exception Exception.
     *
     * @return void
     */
    protected function setUpHttpClient(string $path, string $json, ?Exception $exception)
    {
        $message = $this->httpClient->shouldReceive('request')->with('GET', $path);
        if (!is_null($exception)) {
            $message->andThrow($exception);
        } else {
            $message->andReturn($this->response);
            $this->response->shouldReceive('getBody')->andReturn($json);
        }
    }
}
