<?php

use App\Events\OrderReceived;
use App\Repositories\CustomerRepository;
use App\Repositories\ProductRepository;

class LoyaltyCategoryDiscountTest extends AbstractDiscountTest
{

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();
        $this->app->instance(CustomerRepository::class, $this->customerRepository);
        $this->app->instance(ProductRepository::class, $this->productRepository);
    }

    /**
     * Test granting loyalty discounts.
     *
     * @return void
     */
    public function testLoyaltyCategoryDiscount()
    {
        $order = $this->getOrder(4);
        $order_received = new OrderReceived($order);

        $this->customerRepository->shouldReceive('find')->with(2)->andReturn(['revenue' => '1001']);
        $this->productRepository->shouldReceive('find')->with('A101')->andReturn(
            [
                'id' => 'A101',
                'description' => 'Screwdriver',
                'category' => '1',
                'price' => '9.75',
            ]
        );
        $this->productRepository->shouldReceive('find')->with('A102')->andReturn(
            [
                'id' => 'A102',
                'description' => 'Electric screwdriver',
                'category' => '1',
                'price' => '49.50',
            ]
        );

        event($order_received);

        $this->assertNotEmpty($order_received->getDiscounts());
        $this->assertContains(
            [
                'type' => 'loyalty',
                'min-revenue' => 1000,
                'discount' => '-10%',
            ],
            $order_received->getDiscounts()
        );
        $this->assertContains(
            [
                'type' => 'category',
                'category' => 1,
                'min-products' => 2,
                'discount' => '-20%',
                'product-id' => 'A101',
            ],
            $order_received->getDiscounts()
        );
        $order['items'][0]['total'] = 15.60;
        $order['items'][1]['total'] = 49.50;
        $this->assertEquals($order['items'][0], $order_received->getUpdatedItem(0));
        $this->assertEquals($order['items'][1], $order_received->getUpdatedItem(1));
        $this->assertEquals(58.59, $order_received->getUpdatedTotal());
    }
}
